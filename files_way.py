import os

def get_way():
    return os.path.dirname(__file__)

def create_way(path):
    way = get_way()
    ident = '/'
    if '\\' in way:
        ident = '\\'

    path = path.replace('/', ident)
    way = '{0}{1}{2}'.format(way, ident, path)
    return way
