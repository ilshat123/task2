import os
import traceback

import files_way
from bd import sqlAlch
from pprint import pprint

from txtWorker import TxtWorker
import re

all_files = os.listdir(files_way.create_way('SampleDataSet_customers'))
all_files = [file for file in all_files if '.csv' in file]
print(all_files)

folder = 'SampleDataSet_customers/'


def add_products():
    all_products = set()

    for file in all_files:
        fold_file = files_way.create_way(folder + file)
        text = TxtWorker.read(fold_file)
        product_codes = re.findall(r'PRD\d*', text)
        product_codes = set(product_codes)
        all_products.update(product_codes)

    print(len(all_products))

    results = []

    for product in all_products:
        obj = sqlAlch.Products()
        obj.id = product
        results.append(obj)

    input('fff')
    write_obj = sqlAlch.ConnectBd()

    write_obj.add_objs(results)


def add_baskets():
    all_baskets = set()

    for file in all_files:
        fold_file = files_way.create_way(folder + file)
        text = TxtWorker.read(fold_file)
        basket_column = text.split('\n')[0].split(',')
        targ_index = basket_column.index('BASKET_ID')
        targ_elems = []
        text = text.split('\n')[1:]
        for elem in text:
            elem = elem.split(',')
            try:
                targ_elems.append(elem[targ_index])
            except:
                pass

        product_codes = set(targ_elems)
        all_baskets.update(product_codes)

    print(len(all_baskets))

    results = []
    # input('fff11')

    for basket in all_baskets:
        obj = sqlAlch.Baskets()
        obj.id = int(basket)
        results.append(obj)

    print(len(results))
    input('fff')
    write_obj = sqlAlch.ConnectBd()

    write_obj.add_objs(results)


def add_baskets_products():
    write_obj = sqlAlch.ConnectBd()

    for file in all_files:
        fold_file = files_way.create_way(folder + file)
        text = TxtWorker.read(fold_file)
        basket_column = text.split('\n')[0].split(',')
        basket_id = basket_column.index('BASKET_ID')
        product_code = basket_column.index('PROD_CODE')

        targ_elems = []
        text = text.split('\n')[1:]
        for elem in text:
            elem = elem.split(',')

            try:
                b_id = elem[basket_id]
                prd_id = elem[product_code]
                # print(b_id, prd_id)
                # input('fff')
                obj = sqlAlch.ProductsBaskets()
                obj.basket_id = int(b_id)
                obj.product_id = prd_id
                targ_elems.append(obj)
            except IndexError:
                pass

        write_obj.add_objs(targ_elems)
        print(file)


def create_edges():
    # elems
    pass


# add_products()
# add_baskets()
# add_baskets_products()