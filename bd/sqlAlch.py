from sqlalchemy import create_engine, exc, Column, Integer, String, BigInteger
from sqlalchemy.orm import sessionmaker
from sqlalchemy import func
import typing

# from sqlalchemy
import all_info
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

# from bd.models import Base, Group, GroupPost, PeopleGroup, People, PeopleFriends


class Products(Base):
    __tablename__ = 'dr_products'
    id = Column(String, primary_key=True)


class Baskets(Base):
    __tablename__ = 'dr_baskets'
    id = Column(BigInteger, primary_key=True)


class ProductsBaskets(Base):
    __tablename__ = 'dr_products_baskets'
    product_id = Column(String, primary_key=True)
    basket_id = Column(BigInteger, primary_key=True)


class Edges(Base):
    __tablename__ = 'dr_edges'
    first_top = Column(String, primary_key=True)
    second_top = Column(String, primary_key=True)
    weight = Column(Integer, default=0)


class ConnectBd:
    engine = create_engine(all_info.postgres_bd_way, echo=False)

    def __init__(self):
        self.create_table()
        session = sessionmaker(bind=self.engine, autoflush=False, autocommit=False)
        self.session = session()

    def create_table(self):
        Base.metadata.create_all(self.engine)

    def commit(self):
        self.session.commit()

    def add_objs(self, objs):
        for elem in objs:
            self.session.add(elem)

        # self.session.commit()

    def create_graph(self):
        elems = self.session.query(Baskets).all()
        for elem in elems:
            products = self.session.query(ProductsBaskets).filter(ProductsBaskets.basket_id == elem.id)
            ids = [el.product_id for el in products]
            results = self.session.query(Edges).filter(Edges.first_top.in_(ids), Edges.second_top.in_(ids))
            for res in results:
                res.weight += 1

        self.commit()

    def test_get(self):
        pass
        # objs = self.session.query(ProductsBaskets).filter(ProductsBaskets.product_id == 994100700695424)
        # objs = list(objs)
        # print(len(objs))

    def create_edges(self):
        tops = self.session.query(Products).all()
        for num, top1 in enumerate(tops):
            all_objs = []
            for top2 in tops:
                edge_obj = Edges()
                edge_obj.first_top = top1.id
                edge_obj.second_top = top2.id
                edge_obj.weight = 0
                all_objs.append(edge_obj)
            print(top1)
            self.add_objs(all_objs)
            if num % 100 == 0:
                self.commit()

    def split_on_components_with_targ_num(self, num):
        """Разбивает на компоненты, дает ближайшее число компонент к num"""
        req = self.session.query(func.max(Edges.weight).label("max_score"),
                                 func.min(Edges.weight).label("min_score"))
        max_num = req.max_score
        summ = max_num // 2
        step = max_num // 2

        components = []
        while step != 0:
            components = self.split_on_components(summ)
            step = step // 2
            if len(components) > num:
                summ -= step
            elif len(components) < num:
                summ += step
            else:
                break
        return components

    def split_on_components(self, min_num):
        """Разбивает на компоненты, из каждого ребра вычитает min_num"""
        tops = self.session.query(Products).all()
        tops = [top.id for top in tops]
        all_components = []
        ignore_tops = set()

        for top in tops:
            if top not in ignore_tops:
                component = self.get_component(top, min_num)
                all_components.append(component)
                ignore_tops.update(set(component))
        return all_components

    def get_component(self, first_top, min_num):
        tops = [first_top]
        unique_tops = set(first_top)

        for top in tops:
            new_edges = self.session.query(Edges).filter(Edges.first_top == top, Edges.weight - min_num > 0)
            tops = [edge.second_top for edge in new_edges]

            for elem in tops:
                if elem not in unique_tops:
                    tops.append(elem)
                    unique_tops.add(elem)

        return tops







if __name__ == '__main__':
    obj = ConnectBd()
    obj.test_get()
    obj.create_edges()

    # obj1 = Test()
    # obj1.id = 1
    # obj.test()
    # elems = obj.session.query(AkkPay).filter(AkkPay.added_to_kasma == None)
    # for i in elems:
    #     i.added_to_kasma = True
    # obj.commit()
    # p = obj.get_all_pays()
    # for i in p:
    #     print(repr(i))
